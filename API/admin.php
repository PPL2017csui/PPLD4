<?php
require "config/db.php";

$db = connectDB();
if(isset($_FILES['import'])){
    $import = $db->prepare('INSERT INTO table_batik
              (nama_batik,daerah_batik,makna_batik,harga_rendah,harga_tinggi,hitung_view,link_batik) 
              VALUES (?,?,?,?,?,?,?);
              ');

    $select = $db->prepare('SELECT * FROM table_batik WHERE nama_batik = :nama');
    $delete = $db->prepare('DELETE FROM table_batik WHERE nama_batik = :nama');

    // Check if file is .csv
    $filename = $_FILES['import']['name'];
    $extension = explode('.', $filename);
    $extension = array_pop($extension);

    if (strtolower($extension) == "csv") {
        // Set temp name and then open
        $temp_name = $_FILES['import']['tmp_name'];
        $handle = fopen($temp_name, 'r');

        // Loop through the file and get contents via fgetcsv into an array
        while (($items = fgetcsv($handle, 1000, ',')) !== FALSE) {

            $select->execute(array(':nama'=>$items[0]));
            if($select->rowCount() > 0){
                // Execute prepared query
                $delete->execute(array(':nama'=>$items[0]));
                $import->execute($items);
            }else {
                $import->execute($items);
            }
        }

        // Close the opened file
        fclose($handle);
    } else {
        // Show error message if the file isnt .csv
        echo "Not a CSV file.";
    }
}else if(isset($_POST['export'])){

    $export = $db->prepare('SELECT * FROM table_batik');
    $export->execute();

    $filename = 'table_batik.csv';

    $data = fopen($filename, 'w');

    while ($row = $export->fetch(PDO::FETCH_ASSOC)) {
        fputcsv($data, $row);
    }
    fclose($data);

    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/csv");
    header("Content-Transfer-Encoding: binary");
    readfile($filename);
} else {

}
?>
<html>
<head>
    <style>
        div{
            padding-top:10px;
        }
    </style>
</head>
<body>
<div>
    <H2>Insert New Data</H2>
    <form action="" method="POST" enctype="multipart/form-data">
        <div><input type="file" name="import"/></div>
        <div><input type="submit" name="submit" value="Import"/></div>
    </form>
</div>
<div>
    <H2>Export Table</H2>
    <form action="" method="POST" enctype="multipart/form-data">
        <div><input type="submit" name="export" value="Export"/></div>
    </form>
</div>
</body>
</html>