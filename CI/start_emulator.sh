 #!/bin/bash
 wget --quiet --output-document=android-wait-for-emulator https://raw.githubusercontent.com/travis-ci/travis-cookbooks/0f497eb71291b52a703143c5cd63a217c8766dc9/community-cookbooks/android-sdk/files/default/android-wait-for-emulator
 chmod +x android-wait-for-emulator
 echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter sys-img-x86-google_apis-${ANDROID_TARGET_SDK}
 echo no | android-sdk-linux/tools/android create avd -n test -t android-${ANDROID_TARGET_SDK} --abi google_apis/x86
 android-sdk-linux/tools/emulator64-x86 -avd test -no-window -no-audio &
 ./android-wait-for-emulator
 adb shell input keyevent 82 &