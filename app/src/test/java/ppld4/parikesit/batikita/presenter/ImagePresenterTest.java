package ppld4.parikesit.batikita.presenter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import ppld4.parikesit.batikita.enums.SearchSign;
import ppld4.parikesit.batikita.model.Batik;
import ppld4.parikesit.batikita.model.PageResult;
import ppld4.parikesit.batikita.view.GalleryActivity;

/**
 * Created by Rachel on 5/11/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class ImagePresenterTest {

    @Mock
    private GalleryActivity view;
    private ImagePresenter presenter;

    private Field field_totalElements;
    private Method getRandomQuery;
    private PageResult pageResult;

    @Before
    public void setUp() throws Exception {
        presenter = new ImagePresenter(view);

        getRandomQuery = presenter.getClass().getDeclaredMethod("getRandomQuery");
        getRandomQuery.setAccessible(true);

        pageResult = new PageResult();
        field_totalElements = pageResult.getClass().getDeclaredField("totalElements");
        field_totalElements.setAccessible(true);
    }

    @Test
    public void test_getDummyResult() throws Exception {
        presenter.getDummyResult();
    }

    @Test
    public void test_acceptNewList_1() throws Exception {
        field_totalElements.set(pageResult, 1);

        List<Batik> list = new ArrayList<>();
        list.add(new Batik());

        Field field_result = pageResult.getClass().getDeclaredField("results");
        field_result.setAccessible(true);
        field_result.set(pageResult, list);

        presenter.acceptNewList(pageResult);
    }

    @Test
    public void test_acceptNewList_2() throws Exception {
        field_totalElements.set(pageResult, 0);
        presenter.acceptNewList(pageResult);
    }

    @Test
    public void test_setViewContent_1() throws Exception {
        presenter.setViewContent(SearchSign.NEW_QUERY);
    }

    @Test
    public void test_setViewContent_2() throws Exception {
        presenter.setViewContent(SearchSign.NO_RESULT);
    }

    @Test
    public void test_setViewContent_3() throws Exception {
        presenter.setViewContent(SearchSign.CONNECTION_FAILED);
    }

    @Test
    public void test_setViewContent_4() throws Exception {
        presenter.setViewContent(SearchSign.LOAD_MORE);
    }

    @Test
    public void test_getRandomQuery() throws Exception {
        String query = (String) getRandomQuery.invoke(presenter);
        Assert.assertNotNull(query);
    }
}
