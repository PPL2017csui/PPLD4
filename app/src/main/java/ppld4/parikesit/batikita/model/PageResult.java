package ppld4.parikesit.batikita.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Victor Ardianto on 4/12/2017.
 */

public class PageResult {

    @SerializedName("hasil")
    private List<Batik> results = new ArrayList<>();
    @SerializedName("total_halaman")
    private Integer totalPages;
    @SerializedName("total_element")
    private Integer totalElements;
    @SerializedName("min_price")
    private Integer minPrice;
    @SerializedName("max_price")
    private Integer maxPrice;

    public List<Batik> getResults() {
        return results;
    }

    public Integer getTotalElements() {
        return totalElements;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public Integer getMaxPrice() { return maxPrice; }

    public void setResults(List<Batik> results) {
        this.results = results;
    }
}
