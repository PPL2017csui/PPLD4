package ppld4.parikesit.batikita.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.GeoApiContext;
import com.google.maps.PlacesApi;
import com.google.maps.model.PlaceDetails;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.maps.model.PlacesSearchResponse;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import ppld4.parikesit.batikita.R;
import ppld4.parikesit.batikita.adapter.PlaceAdapter;
import ppld4.parikesit.batikita.presenter.MapsPresenter;

public class MapsActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    private static final int MY_PERMISSION_REQUEST_LOCATION = 99;
    private final LatLng mDefaultLocation = new LatLng(-6.364550, 106.829660);

    private GoogleMap mMap;
    private MapsPresenter presenter;
    private RecyclerView recyclerView;

    public CameraPosition mCameraPosition;
    public GoogleApiClient mGoogleApiClient;
    public Location mLastKnownLocation;
    public SlidingUpPanelLayout mLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }

        setContentView(R.layout.activity_maps);

        TextView t = (TextView) findViewById(R.id.slideup_title);
        t.setText(getString(R.string.loading));

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);

        recyclerView = (RecyclerView) findViewById(R.id.rv_place);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();

        presenter = new MapsPresenter(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        updateLocationUI();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        updateLocationUI();
        getDeviceLocation();
        moveCamera();
        radarSearch();
        setLocationSettings();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != MY_PERMISSION_REQUEST_LOCATION) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        updateLocationUI();
        getDeviceLocation();
        moveCamera();
        radarSearch();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onBackPressed() {
        if (mLayout != null && (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }

    public void addMarker(LatLng batikLoc, String name) {
        mMap.addMarker(new MarkerOptions().position(batikLoc).title(name));
    }

    public void setLocation() {
        if (presenter.checkPermission()) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.setOnMyLocationButtonClickListener(this);
        }
    }

    public void setNoLocation() {
        if (presenter.checkPermission()) {
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
    }

    public void showLoadingText() {
        Toast.makeText(this, "Searching Batik Location...", Toast.LENGTH_SHORT).show();
    }

    public void showResult(List<PlaceDetails> result) {
        TextView t = (TextView) findViewById(R.id.slideup_title);
        t.setText(getString(R.string.batik_nearby));

        PlaceAdapter placesAdapter = new PlaceAdapter(result, this);
        recyclerView.setAdapter(placesAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new SlideInUpAnimator());
    }

    private void getDeviceLocation() {
        if (presenter.checkPermission()) {
            mLastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            moveCamera();
        }
    }

    private void moveCamera() {
        // Set the map's camera position to the current location of the device.
        if (mCameraPosition != null) {
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
        } else if (mLastKnownLocation != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), 13));
        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, 13));
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
    }

    private void radarSearch() {
        List<PlaceDetails> listPlaceDetails = new ArrayList<>();

        try {
            GeoApiContext context = new GeoApiContext().setApiKey(this.getString(R.string.google_maps_key));
            PlacesSearchResponse response = PlacesApi.radarSearchQuery(
                    context,
                    new com.google.maps.model.LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()),
                    3000).name("Batik").await();

            int totalBatikShops = response.results.length;

            if (totalBatikShops > 0) {
                for (int i = 0; i < totalBatikShops; i++) {
                    String placeId = response.results[i].placeId;

                    PlaceDetails placeDetails = PlacesApi.placeDetails(context, placeId).await();
                    com.google.maps.model.LatLng tmp = placeDetails.geometry.location;

                    LatLng batikLoc = new LatLng(tmp.lat, tmp.lng);
                    addMarker(batikLoc, placeDetails.name);

                    placeDetails.rating = presenter.distanceFrom(mLastKnownLocation, batikLoc);
                    listPlaceDetails.add(placeDetails);
                }
            }

            listPlaceDetails = presenter.sortPlacesByDistance(listPlaceDetails);

        } catch (Exception e) {
            showLoadingText();
        }

        showResult(listPlaceDetails);
    }

    public void setLocationSettings() {
        int GPSOff = 0;
        try {
            GPSOff = Settings.Secure.getInt(this.getContentResolver(),Settings.Secure.LOCATION_MODE);
        } catch (Exception e) {

        }

        if (GPSOff == 0) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("You need to turn on your location setting. Turn on your location setting?");
            dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(myIntent, MY_PERMISSION_REQUEST_LOCATION);
                }
            });
            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                }
            });
            dialog.show();
        }
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }

        if (presenter.checkPermission()) {
            setLocation();
        } else {
            setNoLocation();
            mLastKnownLocation = null;
        }
    }
}
