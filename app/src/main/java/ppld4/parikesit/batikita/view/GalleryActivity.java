package ppld4.parikesit.batikita.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.io.IOException;

import io.fabric.sdk.android.Fabric;
import ppld4.parikesit.batikita.R;
import ppld4.parikesit.batikita.interfaces.ImageActivity;
import ppld4.parikesit.batikita.model.Batik;
import ppld4.parikesit.batikita.presenter.ImagePresenter;

public class GalleryActivity extends AppCompatActivity implements ImageActivity{
    public static final int MY_PERMISSION_GALLERY = 99;

    private boolean isNoResult = false;

    private ImagePresenter imagePresenter;
    private ImageView mImage;
    private LinearLayout linelayout_1;
    private LinearLayout linelayout_2;
    private ProgressBar progressBar;
    private TextView text_noReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_gallery);

        imagePresenter = new ImagePresenter(this);

        mImage          = (ImageView) findViewById(R.id.image_forSent);
        linelayout_1    = (LinearLayout) findViewById(R.id.linelayout_1);
        linelayout_2    = (LinearLayout) findViewById(R.id.linelayout_2);
        progressBar     = (ProgressBar) findViewById(R.id.progress_searchImg);
        text_noReturn   = (TextView) findViewById(R.id.text_noReturn);

        progressBar.getIndeterminateDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
        resetView();
        openGallery();
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("debug","Banana Dancing");
        if (requestCode == MY_PERMISSION_GALLERY && resultCode == Activity.RESULT_OK && data != null) {
            Bitmap bitmap = null;

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }

            mImage.setImageBitmap(bitmap);
        } else {
            Toast.makeText(this, "You haven't picked an image", Toast.LENGTH_LONG).show();
        }
    }

    public void reselectImage(View view) {
        openGallery();
    }

    public void sendImage(View view) {
        if (mImage.getDrawable() != null) {
            mImage.setImageDrawable(null);
            linelayout_2.setVisibility(View.GONE);
            linelayout_1.setVisibility(View.VISIBLE);
            text_noReturn.setText("");
            progressBar.setVisibility(View.VISIBLE);
            imagePresenter.getDummyResult();
        } else {
            Toast.makeText(this, "No image for sent", Toast.LENGTH_LONG).show();
        }
    }

    public void showNoResult(String input) {
        linelayout_1.setVisibility(View.VISIBLE);
        linelayout_2.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        text_noReturn.setVisibility(View.VISIBLE);
        text_noReturn.setText(input);
        isNoResult = true;
    }

    @Override
    public void openInfoDetail(Batik mBatik) {
        startActivity(new Intent(this, InfoDetailActivity.class).putExtra("batik", mBatik));
    }


    @Override
    public void onBackPressed() {
        if (isNoResult) {
            resetView();
            isNoResult = false;
        } else {
            super.onBackPressed();
        }
    }

    public void clickLinearLayout1(View view) {
        if (isNoResult) {
            resetView();
            isNoResult = false;
        }
    }

    public void resetView() {
        linelayout_1.setVisibility(View.GONE);
        linelayout_2.setVisibility(View.VISIBLE);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivityForResult(intent, MY_PERMISSION_GALLERY);
    }
}
