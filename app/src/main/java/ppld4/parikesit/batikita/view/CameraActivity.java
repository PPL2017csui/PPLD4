package ppld4.parikesit.batikita.view;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import ppld4.parikesit.batikita.R;
import ppld4.parikesit.batikita.interfaces.ImageActivity;
import ppld4.parikesit.batikita.model.Batik;
import ppld4.parikesit.batikita.presenter.ImagePresenter;

public class CameraActivity extends AppCompatActivity implements ImageActivity{

    public static final int MY_PERMISSION_CAMERA = 99;
    public final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 1;

    private boolean isNoResult = false;

    private ImagePresenter imagePresenter;
    private ImageView mImage;
    private LinearLayout linelayout_1;
    private LinearLayout linelayout_2;
    private ProgressBar progressBar;
    private TextView text_noReturn;
    private Uri mImageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        imagePresenter = new ImagePresenter(this);

        mImage = (ImageView) findViewById(R.id.image_forSent);
        linelayout_1    = (LinearLayout) findViewById(R.id.linelayout_1);
        linelayout_2    = (LinearLayout) findViewById(R.id.linelayout_2);
        progressBar     = (ProgressBar) findViewById(R.id.progress_searchImg);
        text_noReturn   = (TextView) findViewById(R.id.text_noReturn);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkMultiplePermissions(REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS, this);
        } else {
            launchCamera();
        }

    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_PERMISSION_CAMERA && resultCode == Activity.RESULT_OK) {
            this.getContentResolver().notifyChange(mImageUri, null);
            ContentResolver cr = this.getContentResolver();
            Bitmap bitmap;
            try
            {
                bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
                mImage.setImageBitmap(bitmap);
            }
            catch (Exception e)
            {
                Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "You haven't picked an image", Toast.LENGTH_LONG).show();
        }
    }

    private void launchCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo;
        try
        {
            photo = this.createTemporaryFile("picture", ".jpg");
            photo.delete();
        }
        catch(Exception e)
        {
            Toast.makeText(this, "Please check SD card! Image shot is impossible!",
                    Toast.LENGTH_LONG).show();
            return;
        }
        mImageUri = Uri.fromFile(photo);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, MY_PERMISSION_CAMERA);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull
            int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] ==
                        PackageManager.PERMISSION_GRANTED && grantResults[2] ==
                        PackageManager.PERMISSION_GRANTED) {
                    launchCamera();
                } else {
                    boolean showRationale1 = shouldShowRequestPermissionRationale(android.Manifest.
                            permission.CAMERA);
                    boolean showRationale2 = shouldShowRequestPermissionRationale(android.Manifest.
                            permission.READ_EXTERNAL_STORAGE);
                    boolean showRationale3 = shouldShowRequestPermissionRationale(android.Manifest.
                            permission.WRITE_EXTERNAL_STORAGE);
                    if (showRationale1 && showRationale2 && showRationale3) {
                    } else {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.CAMERA},
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkMultiplePermissions(int permissionCode, Context context) {

        String[] PERMISSIONS = {android.Manifest.permission.CAMERA,
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (!hasPermissions(context, PERMISSIONS)) {
            ActivityCompat.requestPermissions((Activity) context, PERMISSIONS, permissionCode);
        } else {
            launchCamera();
        }
    }

    private boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >=
                Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) !=
                        PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private File createTemporaryFile(String part, String ext) throws Exception
    {
        File tempDir= Environment.getExternalStorageDirectory();
        tempDir=new File(tempDir.getAbsolutePath()+"/.temp/");
        if(!tempDir.exists())
        {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    @Override
    public void reselectImage(View view) {
        launchCamera();
        if (mImage.getDrawable() != null) {
            mImage.setImageDrawable(null);
            linelayout_2.setVisibility(View.GONE);
            linelayout_1.setVisibility(View.VISIBLE);
            text_noReturn.setText("");
            progressBar.setVisibility(View.VISIBLE);
            imagePresenter.getDummyResult();
        } else {
            Toast.makeText(this, "No image for sent", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void sendImage(View view) {
        if (mImage.getDrawable() != null) {
            mImage.setImageDrawable(null);
            linelayout_2.setVisibility(View.GONE);
            linelayout_1.setVisibility(View.VISIBLE);
            text_noReturn.setText("");
            progressBar.setVisibility(View.VISIBLE);
            imagePresenter.getDummyResult();
        } else {
            Toast.makeText(this, "No image for sent", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void showNoResult(String input) {
        linelayout_1.setVisibility(View.VISIBLE);
        linelayout_2.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        text_noReturn.setVisibility(View.VISIBLE);
        text_noReturn.setText(input);
        isNoResult = true;
    }

    @Override
    public void openInfoDetail(Batik mBatik) {
        startActivity(new Intent(this, InfoDetailActivity.class).putExtra("batik", mBatik));
    }

    @Override
    public void resetView() {
        linelayout_1.setVisibility(View.GONE);
        linelayout_2.setVisibility(View.VISIBLE);
    }
}
