package ppld4.parikesit.batikita.utils;

import android.os.Handler;

import ppld4.parikesit.batikita.adapter.BatikAdapter;
import ppld4.parikesit.batikita.model.PageResult;

/**
 * Created by Victor Ardianto on 4/17/2017.
 */

public class ListHandler {

    private BatikAdapter batikAdapter;

    public ListHandler(BatikAdapter batikAdapter) {
        this.batikAdapter = batikAdapter;
    }

    public void updateRecyclerView(final PageResult pageResult) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                batikAdapter.addNewList(pageResult.getResults());
                batikAdapter.notifyDataSetChanged();
            }
        }, 1000);
    }
}
