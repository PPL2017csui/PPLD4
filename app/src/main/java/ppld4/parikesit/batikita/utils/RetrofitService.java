package ppld4.parikesit.batikita.utils;

import android.util.Log;

import ppld4.parikesit.batikita.enums.SearchSign;
import ppld4.parikesit.batikita.interfaces.Presenter;
import ppld4.parikesit.batikita.interfaces.RetrofitPresenter;
import ppld4.parikesit.batikita.model.PageResult;
import ppld4.parikesit.batikita.presenter.SearchPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Victor Ardianto on 4/5/2017.
 */

public class RetrofitService {

    private Presenter mPresenter;
//    private Presenter presenter;
    private Call<PageResult> createCall;

    public RetrofitService(Presenter presenter) {
        this.mPresenter = presenter;
    }

    public void setCreateCall(Call<PageResult> createCall) {
        this.createCall = createCall;
    }

    public void doEnqueue() {
        createCall.enqueue(new Callback<PageResult>() {
            @Override
            public void onResponse(Call<PageResult> _, Response<PageResult> resp) {
                Log.d("debug", "Success on doEnqueue()");
                PageResult pageResult = resp.body();
                Log.d("debug", pageResult.getTotalElements()+"");
                mPresenter.acceptNewList(pageResult);
            }

            @Override
            public void onFailure(Call<PageResult> _, Throwable t) {
                Log.d("bla", "Failed on doEnqueue()");
                t.printStackTrace();
                mPresenter.setViewContent(SearchSign.CONNECTION_FAILED);
            }
        });
    }
}
