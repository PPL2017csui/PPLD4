package ppld4.parikesit.batikita.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.maps.model.PlaceDetails;

import java.util.List;
import ppld4.parikesit.batikita.R;

/**
 * Created by George A. Pitoy on 4/19/2017.
 */

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.PlaceViewHolder> {

    public static class PlaceViewHolder extends RecyclerView.ViewHolder {

        public CardView cardPlace;
        public TextView placeName;
        public TextView placeAddr;
        public TextView placeDist;

        public PlaceViewHolder(final View itemView) {
            super(itemView);
            cardPlace = (CardView) itemView.findViewById(R.id.place_card_view);
            placeName = (TextView) itemView.findViewById(R.id.place_name);
            placeAddr = (TextView) itemView.findViewById(R.id.place_addr);
            placeDist = (TextView) itemView.findViewById(R.id.place_dist);
        }
    }

    private List<PlaceDetails> placesList;
    private Context mContext;

    public PlaceAdapter(List<PlaceDetails> placesList, Context mContext) {
        this.placesList = placesList;
        this.mContext = mContext;
    }

    @Override
    public PlaceAdapter.PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View placeView = inflater.inflate(R.layout.activity_maps_place, parent, false);
        PlaceViewHolder viewHolder = new PlaceViewHolder(placeView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PlaceAdapter.PlaceViewHolder viewHolder, int position){
        final PlaceDetails place = placesList.get(position);

        TextView placeNameView = viewHolder.placeName;
        placeNameView.setText(place.name);

        TextView placeAddrView = viewHolder.placeAddr;
        placeAddrView.setText(place.formattedAddress);

        TextView placeDistView = viewHolder.placeDist;
        String distanceVal = String.valueOf(place.rating/1000).substring(0,4) + " km";
        placeDistView.setText(distanceVal);

        viewHolder.cardPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(place.url.toString());
                viewHolder.cardPlace.getContext().startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
        });
    }

    @Override
    public int getItemCount() {
        return placesList.size();
    }

}
