package ppld4.parikesit.batikita.enums;

/**
 * Created by Victor Ardianto on 5/12/2017.
 */

public enum SearchSign {
    NEW_QUERY,
    NO_RESULT,
    CONNECTION_FAILED,
    LOAD_MORE
}
