package ppld4.parikesit.batikita.application;

import android.content.res.Configuration;
import android.support.multidex.MultiDexApplication;

/**
 * Created by geraldo on 6/1/2017.
 */

public class Batikita extends MultiDexApplication{
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
