package ppld4.parikesit.batikita.presenter;

import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.model.PlaceDetails;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ppld4.parikesit.batikita.view.MapsActivity;

/**
 * Created by Victor Ardianto on 4/26/2017.
 */

public class MapsPresenter {

    private static final int MY_PERMISSION_REQUEST_LOCATION = 99;
    private MapsActivity view;

    public MapsPresenter(MapsActivity view) {
        this.view = view;
    }

    public float distanceFrom(Location lastLocation, LatLng placeLocation) {
        double lat1 = lastLocation.getLatitude();
        double lat2 = placeLocation.latitude;

        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(placeLocation.longitude - lastLocation.getLongitude());

        double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng/2) * Math.sin(dLng/2);
        double earthRadius = 6371000; // meters

        return (float) (earthRadius * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)));
    }

    public List<PlaceDetails> sortPlacesByDistance(List<PlaceDetails> listPlaces) {
        Collections.sort(listPlaces, new Comparator<PlaceDetails>() {
            public int compare(PlaceDetails p1, PlaceDetails p2) {
                if (p1.rating > p2.rating) return 1;
                if (p1.rating < p2.rating) return -1;
                return 0;
            }});
        return listPlaces;
    }

    public boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(view, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(view,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSION_REQUEST_LOCATION);
        }
        return false;
    }
}
